package com.locationCoupon.yossi.locann;

/**
 * Created by yossi on 1/14/2015.
 */
public class Lock{

    private boolean isLocked = false;

    public synchronized void lock()
            throws InterruptedException{
        while(isLocked){
            wait();
        }
        isLocked = true;
    }

    public synchronized void unlock(){
        isLocked = false;
        notify();
    }
}