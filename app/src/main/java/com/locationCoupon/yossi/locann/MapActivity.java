package com.locationCoupon.yossi.locann;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;
import java.util.Vector;

public class MapActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener, DbListeners, NumberPicker.OnValueChangeListener, AdapterView.OnItemSelectedListener, GoogleMap.OnMapLoadedCallback, LocationListeners {

    private ServiceManagerBroadcastReceiver service;
    private Context context;
    private ServiceDb db;
    private GoogleMap googleMap;
    private int days, mins, hours;
    private String res;
    private ServiceLocation location;
    private boolean isAnimated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        context = MapActivity.this;
        isAnimated = false;

        db = new ServiceDb(context, this);
        db.getWritableDatabase();

        service = new ServiceManagerBroadcastReceiver();

        NumberPicker mins = (NumberPicker) findViewById(R.id.mins);
        mins.setMaxValue(59);
        mins.setMinValue(0);
        mins.setValue(0);
        mins.setOnValueChangedListener(this);

        NumberPicker hours = (NumberPicker) findViewById(R.id.hours);
        hours.setMaxValue(23);
        hours.setMinValue(0);
        hours.setValue(0);
        hours.setOnValueChangedListener(this);

        Spinner days = (Spinner) findViewById(R.id.days);
        ArrayAdapter<CharSequence> adapterDays = ArrayAdapter.createFromResource(this, R.array.days_array, android.R.layout.simple_spinner_item);
        adapterDays.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        days.setAdapter(adapterDays);
        days.setOnItemSelectedListener(this);

        Spinner res = (Spinner) findViewById(R.id.res);
        ArrayAdapter<CharSequence> adapterRes = ArrayAdapter.createFromResource(this, R.array.res_array, android.R.layout.simple_spinner_item);
        adapterRes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        res.setAdapter(adapterRes);
        res.setOnItemSelectedListener(this);

        this.mins= 0;
        this.hours=0;
        this.days= 0;
        this.res="all";

        findViewById(R.id.days).setVisibility(View.GONE);
        findViewById(R.id.mins).setVisibility(View.GONE);
        findViewById(R.id.hours).setVisibility(View.GONE);
        findViewById(R.id.separate).setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Vector<Destination> destinations = db.getAllDestinations();
        for (Destination destination : destinations) {
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(destination.coord.latitude, destination.coord.longitude))
                    .title(destination.coord.latitude+" "+ destination.coord.longitude))
                    .setSnippet(String.valueOf(destination.id));
        }

        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMapLoadedCallback(this);
        this.googleMap = googleMap;

        location = new ServiceLocation(context,  ServiceClock.getCurrentTimeMili(), 300, this);
    }

    public void onMapLongClick(final LatLng latlan) {
//
//        final Dialog dialog = new Dialog(this);
//        dialog.setContentView(R.layout.activity_add_destination);
//        dialog.setTitle("Add Destination");
//
//        Button addDestBtn = (Button) dialog.findViewById(R.id.Add);
//        Button cancelBtn = (Button) dialog.findViewById(R.id.Cancel);
//        final SeekBar seekbar = (SeekBar) dialog.findViewById(R.id.SeekBar);
//        final EditText purpose = (EditText) dialog.findViewById(R.id.Purpose);
//        final RadioButton arrival = (RadioButton) dialog.findViewById(R.id.CheckIn);
//        final RadioButton notifyFreq = (RadioButton) dialog.findViewById(R.id.Single);
//        arrival.setChecked(true);
//        notifyFreq.setChecked(true);
//        addDestBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean inOut = arrival.isChecked();
//                int status = notifyFreq.isChecked() ? 1 : 2;
//
//                db.addDestination(new Destination(new Coordinate(latlan.latitude, latlan.longitude, 50), seekbar.getProgress(), inOut, status, purpose.getText().toString()));
//                //start iteration when inserting new destination
//                service.setAlarm(context,0);
//                dialog.cancel();
//                googleMap.clear();
//                updateMap();
//            }
//        });
//        cancelBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.cancel();
//            }
//        });
//        dialog.show();
    }

    @Override
    public void onDbOpened() {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Intent intent = new Intent(context, OfferScreen.class);
        intent.putExtra("isFromMap",true);
        intent.putExtra("id",db.getDestination(Integer.parseInt(marker.getSnippet())).name);
        intent.putExtra("lng",marker.getPosition().longitude);
        intent.putExtra("lat",marker.getPosition().latitude);
        startActivity(intent);
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//        alertDialogBuilder.setTitle("Edit destination");
//        alertDialogBuilder
//                .setMessage("What do you want to do?")
//                .setCancelable(false)
//                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        db.removeDestination(Integer.parseInt(marker.getSnippet()));
//                        dialog.cancel();
//                        googleMap.clear();
//                        updateMap();
//                    }
//                })
//                .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, int id) {
//                        final Destination dest = db.getDestination(Integer.parseInt(marker.getSnippet()));
//                        final Dialog editDialog = new Dialog(context);
//                        editDialog.setContentView(R.layout.activity_edit_destination);
//
//
//                        Button saveDestBtn = (Button) editDialog.findViewById(R.id.Save);
//                        Button cancelBtn = (Button) editDialog.findViewById(R.id.Cancel);
//                        final SeekBar seekbar = (SeekBar) editDialog.findViewById(R.id.SeekBar);
//                        final EditText purpose = (EditText) editDialog.findViewById(R.id.Purpose);
//                        final RadioButton arrival = (RadioButton) editDialog.findViewById(R.id.CheckIn);
//                        final RadioButton leaving = (RadioButton) editDialog.findViewById(R.id.CheckOut);
//                        final RadioButton notifyFreqSingle = (RadioButton) editDialog.findViewById(R.id.Single);
//                        final RadioButton notifyFreqRepeat = (RadioButton) editDialog.findViewById(R.id.Repeat);
//                        purpose.setText(dest.name);
//                        seekbar.setProgress(dest.importance);
//                        if (dest.inOut) {
//                            arrival.setChecked(true);
//                            leaving.setChecked(false);
//                        } else {
//                            leaving.setChecked(true);
//                            arrival.setChecked(false);
//                        }
//                        if (dest.status == 0) {
//                            notifyFreqSingle.setChecked(false);
//                            notifyFreqRepeat.setChecked(false);
//                        } else if (dest.status == 1) {
//                            notifyFreqSingle.setChecked(true);
//                            notifyFreqRepeat.setChecked(false);
//                        } else if (dest.status == 2) {
//                            notifyFreqSingle.setChecked(false);
//                            notifyFreqRepeat.setChecked(true);
//                        }
//                        saveDestBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                boolean inOut = arrival.isChecked() ? true : false;
//                                int status = notifyFreqSingle.isChecked() ? 1 : 2;
////                                Toast.makeText(context, "text: "+purpose.getText().toString()+" importance: "+seekbar.getProgress()+" inout: "+inOut+" status: "+status , Toast.LENGTH_SHORT).show();
//                                dest.importance = seekbar.getProgress();
//                                dest.inOut = inOut;
//                                dest.status = status;
//                                dest.name = purpose.getText().toString();
//                                db.saveDestination(dest);
//                                editDialog.cancel();
//                            }
//                        });
//                        cancelBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                editDialog.cancel();
//                            }
//                        });
//                        editDialog.show();
//                        editDialog.setTitle("Edit Destination");
//                        dialog.cancel();
//                        googleMap.clear();
//                        updateMap();
//                    }
//                });
//
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        alertDialog.show();
        return true;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        if(picker.getId()==R.id.mins) {
            mins = newVal;
        }
        if(picker.getId()==R.id.hours) {
            hours = newVal;
        }

        updateMap();
    }

    private void updateMap() {
        googleMap.clear();
        onMapReady(googleMap);

        Vector<LocationProb> locs = db.getLocationsProbs(days, hours, mins, res, 5);
//        int[] colors = {
//                Color.argb(255, 102, 225, 0), // green
//                Color.argb(255, 255, 225, 0), // yellow
//                Color.argb(255, 255, 0, 0)    // red
//        };
//        float[] startPoints = {0.01f, 0.1f, 0.7f};
//        Gradient gradient = new Gradient(colors, startPoints);
        Vector<WeightedLatLng> locsLatLng = new Vector<WeightedLatLng>();

        for (LocationProb loc : locs) {
            locsLatLng.add(new WeightedLatLng(new LatLng(loc.coord.latitude, loc.coord.longitude), loc.p));
        }
        if (locsLatLng.size()>0) {
            HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                    .weightedData(locsLatLng)
//                .gradient(gradient)
                    .build();
            TileOverlay mOverlay = this.googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            mProvider.setOpacity(1.0);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object val = parent.getItemAtPosition(position);

        if(parent.getId()==R.id.res) {
            if (val.toString().equals("All")) {
                res = "all";
                mins = 0;
                hours = 0;
                days = 0;
                findViewById(R.id.days).setVisibility(View.GONE);
                findViewById(R.id.mins).setVisibility(View.GONE);
                findViewById(R.id.hours).setVisibility(View.GONE);
                findViewById(R.id.separate).setVisibility(View.GONE);
            } else if (val.toString().equals("Day")) {
                res = "day";
                days = 0;
                findViewById(R.id.days).setVisibility(View.GONE);
                findViewById(R.id.mins).setVisibility(View.VISIBLE);
                findViewById(R.id.hours).setVisibility(View.VISIBLE);
                findViewById(R.id.separate).setVisibility(View.VISIBLE);

            } else if (val.toString().equals("Week")) {
                res = "week";
                findViewById(R.id.days).setVisibility(View.VISIBLE);
                findViewById(R.id.mins).setVisibility(View.VISIBLE);
                findViewById(R.id.hours).setVisibility(View.VISIBLE);
                findViewById(R.id.separate).setVisibility(View.VISIBLE);
            }
        }
        if(parent.getId()==R.id.days) {
            if (val.toString().equals("Sunday")) {
                days=3;
            } else if (val.toString().equals("Monday")) {
                days=4;
            } else if (val.toString().equals("Tuesday")) {
                days=5;
            } else if (val.toString().equals("Wednesday")) {
                days=6;
            } else if (val.toString().equals("Thursday")) {
                days=0;
            } else if (val.toString().equals("Friday")) {
                days=1;
            } else if (val.toString().equals("Saturday")) {
                days=2;
            }
        }
        updateMap();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onLocationReceived(Coordinate coord) {
        if(!isAnimated) {
            isAnimated = true;
            LatLng myLocation = new LatLng(coord.latitude, coord.longitude);
            this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, (float) 13));
        }
    }

    @Override
    public void onLocationFailed() {

    }
}