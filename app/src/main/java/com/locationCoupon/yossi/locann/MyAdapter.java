package com.locationCoupon.yossi.locann;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyAdapter extends ArrayAdapter<Offer> {
    public MyAdapter(Context context, int textViewResourceId, List<Offer> objects) {
        super(context, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.coupon_layout, parent, false);

        //get objects
        int nameBound = Math.min(30, getItem(position).name.length());
        int descBound = Math.min(55, getItem(position).description.length());

        TextView name = (TextView)view.findViewById(R.id.name);
        TextView description = (TextView)view.findViewById(R.id.description);
        TextView price = (TextView)view.findViewById(R.id.price);
        TextView distance = (TextView)view.findViewById(R.id.distance);

        if (getItem(position).name.equals("No interests found, Go to Settings")) {
            name.setText("No interests found, Go to Settings");
            name.setTextSize(20);
            description.setText("");
            price.setText("");
            distance.setText("");
            return view;
        }

        if (getItem(position).name.equals("No results found")) {
            name.setText("No results found");
            name.setTextSize(20);
            description.setText("");
            price.setText("");
            distance.setText("");
            return view;
        }

        name.setText(getItem(position).name.substring(0,nameBound));
        description.setText(getItem(position).description.substring(0,descBound));
        price.setText(Integer.toString(getItem(position).price)+" NIS");
        if (getItem(position).distance>=0){
            distance.setText(Integer.toString(getItem(position).distance) + " Km away");
        } else {
            distance.setText("");
        }

        return view;
    }
}

