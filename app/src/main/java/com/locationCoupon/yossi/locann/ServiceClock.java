package com.locationCoupon.yossi.locann;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by yossi on 1/1/2015.
 */
public class ServiceClock {
    public static long getCurrentTimeMili() {
        int gmtOffset = TimeZone.getDefault().getRawOffset();
        return System.currentTimeMillis() + gmtOffset;
    }

    public static long getCurrentTimeSec() {
        return getCurrentTimeMili()/1000;
    }

    public static long getCurrentTimeMin() {
        return getCurrentTimeSec()/60;
    }

    public static String getHumanTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM HH:mm");

        Date resultdate = new Date( System.currentTimeMillis());
        return sdf.format(resultdate);
    }

    public static String getHumanTime(long epoch) {
        int gmtOffset = TimeZone.getDefault().getRawOffset();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM HH:mm");
        Date resultdate = new Date(epoch - gmtOffset);
        return sdf.format(resultdate);
    }
}
