package com.locationCoupon.yossi.locann;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServiceManagerActivity extends ActionBarActivity implements LocationListeners, DbListeners {
    boolean isFirstView = true;
    int counter = 0;
    boolean isLocationReceived = false;
    public Context context;
    private ServiceManagerBroadcastReceiver service;
    public static TelephonyManager telephonyManager;
    private ServiceLog log;
    public static String myUserId;
    private ServiceDb db;
    final int RQS_GooglePlayServices = 10;
    ListView items;
    List<Offer> listData;
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_manager);

        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        context = this.getApplicationContext();

        EditText et = (EditText) findViewById(R.id.search);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                counter++;
                String str = editable.toString();
                AsyncHttpClient client = new AsyncHttpClient();
                String url = "http://yossavi.cloudapp.net/forus/api/coupon?where={\"or\":[{\"company\":{\"contains\":\""+str+"\"}},{\"description\":{\"contains\":\""+str+"\"}},{\"name\":{\"contains\":\""+str+"\"}}]}";
                listData = new ArrayList<Offer>();
                client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        --counter;
                        if (counter == 0 ) {
                            String res = new String(bytes);
                            try {
                                JSONArray json = new JSONArray(res);
                                if (json.length() == 0) {
                                    Toast.makeText(context, "No results found", Toast.LENGTH_SHORT).show();
                                    listData.add(new Offer("No results found", "", "", 0, 0, "", 0, 0));
                                    adapter = new MyAdapter(getApplicationContext(), R.layout.coupon_layout, listData);
                                    items.setAdapter(adapter);
                                } else {
                                    int index = 0;
                                    while (!json.isNull(index)) {
                                        try {
                                            JSONObject JO = json.getJSONObject(index);
                                            Offer offer = new Offer(JO.getString("name"), JO.getString("description"), JO.getString("address"), -1, JO.getInt("price"), JO.getString("category"), JO.getDouble("lat"), JO.getDouble("lng"));
                                            listData.add(offer);
                                        } catch (JSONException e) {

                                        }
                                        index++;
                                    }
                                    adapter = new MyAdapter(getApplicationContext(), R.layout.coupon_layout, listData);
                                    items.setAdapter(adapter);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                onFailure(-1, new Header[1], new byte[1], e);
                            }
                        }
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        afterTextChanged(editable);
                    }
                });
            }
        });


        service = new ServiceManagerBroadcastReceiver();
        service.setAlarm(context, 120);
        db = new ServiceDb(context, this);
        db.getWritableDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_service_manager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateOffers(final Coordinate coord){
        items = (ListView) findViewById(R.id.listItems);
        db.clearAllDestinations();
        AsyncHttpClient client = new AsyncHttpClient();
        String url;
        listData = new ArrayList<Offer>();
        if (coord != null) {
            url = "http://yossavi.cloudapp.net/forus/api/user/"+telephonyManager.getDeviceId()+"/"+coord.latitude+"/"+coord.longitude;
        } else {
            url = "http://yossavi.cloudapp.net/forus/api/user/"+telephonyManager.getDeviceId()+"/null/null";
        }
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String res = new String(responseBody);
                try {
                    if (statusCode == 200) {
                        JSONObject json = new JSONObject(res);
                        myUserId = json.getString("id");
                        JSONArray jArr = json.getJSONArray("allCoupons");
                        if (jArr.length() == 0) {
                            //no interests :(
                        }
                        int i = 0;
                        while (!jArr.isNull(i)) {
                            try {
                                JSONObject JO = jArr.getJSONObject(i);
                                Offer offer = new Offer(JO.getString("name"), JO.getString("description"), JO.getString("address"), JO.getInt("distance"), JO.getInt("price"), JO.getString("category"), JO.getDouble("lat"), JO.getDouble("lng"));
                                listData.add(offer);

                                db.addDestination(new Destination(new Coordinate(JO.getDouble("lat"), JO.getDouble("lng"), 50), 50, true, 1, JO.getString("id")));
                            } catch (JSONException e) {

                            }
                            i++;
                        }
                        if (i == 0) {
                            listData.add(new Offer("No interests found, Go to Settings", "", "", 0, 0, "", 0, 0));
                        }
                    }
                    adapter = new MyAdapter(getApplicationContext(), R.layout.coupon_layout, listData);
                    items.setAdapter(adapter);

                    //set click listener
                    items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            if (listData.get(i).name.equals("No interests found, Go to Settings") || listData.get(i).name.equals("No results found")) {
                                return;
                            }
                            Intent intent = new Intent(context, OfferScreen.class);
                            Offer offer;
                            offer = listData.get(i);
                            intent.putExtra("isFromMap", false);
                            intent.putExtra("name", offer.name);
                            intent.putExtra("description", offer.description);
                            intent.putExtra("address", offer.address);
                            intent.putExtra("price", offer.price);
                            intent.putExtra("distance", offer.distance);
                            intent.putExtra("lng", offer.lng);
                            intent.putExtra("lat", offer.lat);
                            startActivity(intent);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                    onFailure(-1, new Header[1], new byte[1], e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                if (statusCode == 404) {
//                    telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//                    351612060671747
                    postMyId();
//                    Intent intent = new Intent(context, Settings.class);
//                    startActivity(intent);
                } else {
//                    updateOffers(coord);
                }
            }

            @Override
            public void onRetry(int retryNo) {
                Toast.makeText(context, "Retrying...", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void postMyId() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("imei", telephonyManager.getDeviceId());
        AsyncHttpResponseHandler AHRH = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Toast.makeText(context, "Success login", Toast.LENGTH_SHORT).show();


                String url = "http://yossavi.cloudapp.net/forus/api/user/"+telephonyManager.getDeviceId()+"/null/null";
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        String res = new String(bytes);
                        try {
                            JSONObject json = new JSONObject(res);
                            myUserId = json.getString("id");
                            Intent intent = new Intent(context, Settings.class);
                            startActivity(intent);
                        } catch (JSONException e){

                        }
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {

                    }
                });


            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(context, "Failure login", Toast.LENGTH_SHORT).show();
            }
        };
        client.post("http://yossavi.cloudapp.net/forus/api/user", params, AHRH);
    }

    @Override
    public void onLocationReceived(Coordinate coord) {
        if (!isLocationReceived) {
            updateOffers(coord);
            isFirstView = false;
            isLocationReceived = true;
        }
    }

    @Override
    public void onLocationFailed() {
        Toast.makeText(context, "Can't get your location", Toast.LENGTH_SHORT).show();
        isFirstView = false;
        updateOffers(null);
    }

    @Override
    public void onDbOpened() {
        new ServiceLocation(context,  ServiceClock.getCurrentTimeMili(), 300, this);
//        updateOffers(null);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        isLocationReceived = false;
//        if (!isFirstView) {
            new ServiceLocation(context, ServiceClock.getCurrentTimeMili(), 300, this);
//        }
        //only available when on emulator
//        updateOffers(null);
    }

}
