package com.locationCoupon.yossi.locann;

public class Offer {
    public String name;
    public String description;
    public String address;
    public String category;
    public double lng;
    public double lat;
    public int distance;
    public int price;

    public Offer(String name,String description, String address, int distance, int price, String category, double lat, double lng){
        this.name = name;
        this.description = description;
        this.address = address;
        this.distance = distance;
        this.price = price;
        this.category = category;
        this.lat = lat;
        this.lng = lng;
    }
}
