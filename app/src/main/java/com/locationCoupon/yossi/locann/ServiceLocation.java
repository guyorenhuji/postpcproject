package com.locationCoupon.yossi.locann;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.TimeZone;

/**
 * Created by yossi on 12/15/2014.
 */
public class ServiceLocation implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Context context;
    private long startTimeMili;
    private double requiredAccuracy;
    private LocationListeners serviceListener;
    private ServiceLog log;
    private boolean tryAgain;

    ServiceLocation(Context context, long startTimeMili, double requiredAccuracy, LocationListeners serviceListener) {
        this.context = context;
        this.startTimeMili = startTimeMili;
        this.requiredAccuracy = requiredAccuracy;
        this.serviceListener = serviceListener;
        this.log = new ServiceLog(context);
        this.tryAgain = true;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    public void stopLocationService() {
        if (mGoogleApiClient!=null) {
            mGoogleApiClient.disconnect();
        }
        log.log("google location services stop");
    }

    @Override
    public void onConnected(Bundle bundle) {
        log.log("google location services connected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100);
        mLocationRequest.setExpirationDuration(50000);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        log.log("google location services connection suspended");
        Toast.makeText(context, "google location services connection suspended", Toast.LENGTH_LONG).show();
        tryAgain = false;
        serviceListener.onLocationFailed();
    }

    @Override
    public void onLocationChanged(Location location) {
        log.log("got location-" + location.getLatitude() + ", " + location.getLongitude() + ", accur:" + location.getAccuracy()+" req: "+requiredAccuracy);
        int gmtOffset = TimeZone.getDefault().getRawOffset();
        long curTimeMili = System.currentTimeMillis() + gmtOffset;

        if (curTimeMili-startTimeMili> 30000 ||  location.getAccuracy()< requiredAccuracy) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            tryAgain = false;
            serviceListener.onLocationReceived(new Coordinate(location.getLatitude(), location.getLongitude(), 50));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        log.log("google location services connection failed");
        Toast.makeText(context, "google location services connection failed", Toast.LENGTH_LONG).show();
        tryAgain = false;
        serviceListener.onLocationFailed();
    }

}
