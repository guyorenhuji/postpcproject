package com.locationCoupon.yossi.locann;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Settings extends ActionBarActivity{
    private ServiceManagerBroadcastReceiver service;
    private Context context;
    private ServiceLog log;
    public static TelephonyManager telephonyManager;
    ListView items;
    List<Interest> listData;
    InterestAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        context = this.getApplicationContext();
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        updateInterests();

        items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, final long l) {
                adapter = new InterestAdapter(context, R.layout.interest_layout, listData);
                items.setAdapter(adapter);

                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                AsyncHttpResponseHandler AHRH = new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        Toast.makeText(context, "Category edited Successfully", Toast.LENGTH_SHORT).show();
                        listData.get((int)l).isInterested = !listData.get((int)l).isInterested;
                        adapter = new InterestAdapter(context, R.layout.interest_layout, listData);
                        items.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                        Toast.makeText(context, "Failure edit category", Toast.LENGTH_SHORT).show();
                    }
                };
                if (listData.get((int)l).isInterested){
                    client.delete("http://yossavi.cloudapp.net/forus/api/user/" + ServiceManagerActivity.myUserId + "/categories/" + listData.get((int) l).id, params, AHRH);
                } else {
                    client.post("http://yossavi.cloudapp.net/forus/api/user/" + ServiceManagerActivity.myUserId + "/categories/" + listData.get((int) l).id, params, AHRH);
                }
            }
        });
    }

    public void startService(View view) {
        if (service != null) {
            service.setAlarm(context, 0);
        } else {
            log.log("Alarm is null");
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelService(View view) {
        if (service != null) {
            service.cancelAlarm(context);
        } else {
            log.log("Alarm is null");
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }
    }

    public void showMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    private void updateInterests(){
        items = (ListView) findViewById(R.id.listItems);
        listData = new ArrayList<Interest>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://yossavi.cloudapp.net/forus/api/category", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String res = new String(responseBody);
                try {
                    JSONArray json = new JSONArray(res);
                    int i = 0;
                    while (!json.isNull(i)) {
                        Interest interest = new Interest(json.getJSONObject(i).getString("id"), json.getJSONObject(i).getString("name"));
                        listData.add(interest);
                        ++i;
                    }
                    updateMyInterests();
                } catch (JSONException e) {
                    e.printStackTrace();
                    onFailure(-1, new Header[1], new byte[1], e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Toast.makeText(context, "Failed connecting to remote DB", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRetry(int retryNo) {
                Toast.makeText(context, "Retry", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateMyInterests() {
        AsyncHttpClient client = new AsyncHttpClient();
        final List<String> updated = new ArrayList<String>();
        client.get("http://yossavi.cloudapp.net/forus/api/user?where={\"imei\":\"" + telephonyManager.getDeviceId() + "\"}&populate=categories", new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String res = new String(responseBody);
                try {
                    JSONArray json = new JSONArray(res).getJSONObject(0).getJSONArray("categories");
                    int i = 0;
                    while (!json.isNull(i)) {
                        String id = json.getJSONObject(i).getString("id");
                        updated.add(id);
                        ++i;
                    }
                    for (int j = 0; j < updated.size(); j++) {
                        for (int k = 0; k < listData.size(); k++) {
                            if (updated.get(j).equals(listData.get(k).id)) {
                                listData.get(k).isInterested = true;
                            }
                        }
                    }
                    adapter = new InterestAdapter(context, R.layout.interest_layout, listData);
                    items.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    onFailure(-1, new Header[1], new byte[1], e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Toast.makeText(context, "Failed connecting to remote DB", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRetry(int retryNo) {
                Toast.makeText(context, "Retry", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void postMyId() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("imei", telephonyManager.getDeviceId());
        AsyncHttpResponseHandler AHRH = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Toast.makeText(context, "Success login", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(context, "Failure login", Toast.LENGTH_SHORT).show();
            }
        };
        client.post("http://yossavi.cloudapp.net/forus/api/user", params, AHRH);
    }

    public void doneSettings(View view){
        Intent i = new Intent(context, ServiceManagerActivity.class);
        startActivity(i);
    }
}

