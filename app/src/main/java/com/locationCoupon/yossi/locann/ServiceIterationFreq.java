package com.locationCoupon.yossi.locann;

/**
 * Created by yossi on 1/1/2015.
 */
public class ServiceIterationFreq {
    public long timeDay;
    public int count;

    public ServiceIterationFreq(long timeDay, int count) {
        this.timeDay = timeDay;
        this.count = count;
    }
}
