package com.locationCoupon.yossi.locann;

/**
 * Created by yossi on 12/17/2014.
 */

import android.location.Location;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


public class ServiceDistance
{
    private DistanceListeners serviceListener;
    private boolean sent;
    private int distanceInMeters;
    private Destination destination;

    public ServiceDistance(DistanceListeners serviceListener) {
        this.serviceListener = serviceListener;
        this.sent = false;
    }

    public void getArrivalTimeInSec(Coordinate origin, final Destination destination) {
        this.destination = destination;
        this.distanceInMeters = getDistanceInMeters(origin, destination.coord);

//        AsyncHttpClient client = new AsyncHttpClient();
//        client.get("https://maps.googleapis.com/maps/api/distancematrix/json"+
//                "?origins="+origin.latitude+","+origin.longitude
//                +"&destinations="+destination.coord.latitude+","+destination.coord.longitude
//                +"&mode=driving&language=en&units=metric"
//                +"&api=AIzaSyCOJgjjP4S3fWgDriPH5vItJNXbhJHEebA", new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onStart() {
//                // called before request is started
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                if(sent) {
//                    return;
//                }
//                sent = true;
//                String s2 = new String(responseBody);
//                try {
//                    JSONObject json  = new JSONObject(s2);
//                    serviceListener.onDistanceReceived(json.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration").getInt("value"), distanceInMeters, destination);
//                } catch (JSONException e) {
//                    onFailure(-1, new Header[1], new byte[1],  e);
//                }
//            }
//
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
//                if(sent) {
//                    return;
//                }
//                sent = true;
//                serviceListener.onDistanceReceived(distanceInMeters/20, distanceInMeters, destination);
//            }
//
//            @Override
//            public void onRetry(int retryNo) {
//                if(sent) {
//                    return;
//                }
//                sent = true;
                serviceListener.onDistanceReceived(distanceInMeters/20, distanceInMeters, destination);
//            }
//        });
    }

    public static int getDistanceInMeters(Coordinate from, Coordinate to) {
        Location loc1 = new Location("");
        loc1.setLatitude(from.latitude);
        loc1.setLongitude(from.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(to.latitude);
        loc2.setLongitude(to.longitude);

        return (int)loc1.distanceTo(loc2);
    }
}