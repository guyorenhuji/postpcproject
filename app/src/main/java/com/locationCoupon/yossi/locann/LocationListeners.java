package com.locationCoupon.yossi.locann;

/**
 * Created by yossi on 12/17/2014.
 */
public interface LocationListeners {
    void onLocationReceived(Coordinate coord);
    void onLocationFailed();
}
