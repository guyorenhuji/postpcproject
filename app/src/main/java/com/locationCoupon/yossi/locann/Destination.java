package com.locationCoupon.yossi.locann;

public class Destination {
	public int id;
	public Coordinate coord;
	public int importance;
	public boolean inOut;
    public String name;
    public int status;
	
	public Destination (Coordinate coord, int importance, boolean inOut, int status, String name) {
		this.coord= coord;
		this.importance = importance;
		this.inOut = inOut;
		this.id = -1;
        this.name = name;
        this.status = status;
	}

    public Destination (Coordinate coord, int importance, boolean inOut, int status, String name, int id) {
        this.coord= coord;
        this.importance = importance;
        this.inOut = inOut;
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public Destination (Coordinate coord, int importance, boolean inOut, int status) {
        this.coord= coord;
        this.importance = importance;
        this.inOut = inOut;
        this.id = -1;
        this.name = "";
        this.status = status;
    }

    public Destination (Coordinate coord, int importance, boolean inOut, int status, int id) {
        this.coord= coord;
        this.importance = importance;
        this.inOut = inOut;
        this.id = id;
        this.name = "";
        this.status = status;
    }


}
