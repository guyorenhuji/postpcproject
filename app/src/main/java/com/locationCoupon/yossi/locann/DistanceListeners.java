package com.locationCoupon.yossi.locann;

import org.json.JSONObject;

/**
 * Created by yossi on 1/14/2015.
 */
public interface DistanceListeners {
    void onDistanceReceived(int arrivalInSec, int distanceInMeters, Destination destination);
}
