package com.locationCoupon.yossi.locann;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class OfferScreen extends ActionBarActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, LocationListeners {

    private Context context;
    GoogleMap googleMap;
    Coordinate myLocation;
    private ServiceLocation location;
    String name;
    double lng;
    double lat;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offer_screen_activity);
        context = this.getApplicationContext();
        Intent intent = getIntent();
        if (intent.getBooleanExtra("isFromMap",false)) {
            String id = intent.getStringExtra("id");
            System.out.print("Yossi offer scrrennn  " + id);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://yossavi.cloudapp.net/forus/api/coupon/" + id, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, Header[] headers, byte[] bytes) {
                    String res = new String(bytes);
                    try {
                        JSONObject jo = new JSONObject(res);
                        ((TextView) findViewById(R.id.name)).setText(jo.getString("name"));
                        ((TextView) findViewById(R.id.description)).setText(jo.getString("description"));
                        ((TextView) findViewById(R.id.address)).setText(jo.getString("address"));
                        ((TextView) findViewById(R.id.distance)).setText("");
                        ((TextView) findViewById(R.id.price)).setText(jo.getInt("price")+" NIS");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    ((TextView) findViewById(R.id.name)).setText("Sorry, coupon doesn't exist anymore");
                    ((TextView) findViewById(R.id.description)).setText("");
                    ((TextView) findViewById(R.id.address)).setText("");
                    ((TextView) findViewById(R.id.distance)).setText("");
                    ((TextView) findViewById(R.id.price)).setText("");
                }
            });
        } else {
            name = intent.getStringExtra("name");
            ((TextView) findViewById(R.id.name)).setText(name);
            ((TextView) findViewById(R.id.description)).setText(intent.getStringExtra("description"));
            ((TextView) findViewById(R.id.address)).setText(intent.getStringExtra("address")+"");

            ((TextView) findViewById(R.id.price)).setText(intent.getIntExtra("price", 0) + " NIS");
            if(intent.getIntExtra("distance", 0)>=0) {
                ((TextView) findViewById(R.id.distance)).setText(intent.getIntExtra("distance", 0) + " KM away");
            } else {
                ((TextView) findViewById(R.id.distance)).setText("");
            }

        }
            lng = intent.getDoubleExtra("lng", 0);
            lat = intent.getDoubleExtra("lat", 0);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    public void showMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //show coupon on map
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title(name));
        location = new ServiceLocation(context,  ServiceClock.getCurrentTimeMili(), 300, this);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), (float) 13));
        googleMap.setOnMapLoadedCallback(this);
        this.googleMap = googleMap;
    }

    @Override
    public void onLocationReceived(Coordinate coord) {
        googleMap.setMyLocationEnabled(true);
        this.myLocation = coord;
//        drawPath(jp.getJSONFromUrl(makeURL(coord.latitude, coord.longitude, lat,lng)));
    }

    @Override
    public void onLocationFailed() {
        Toast.makeText(context, "Failed to get your location", Toast.LENGTH_SHORT).show();
    }

    public void navigateToCoupon(View view){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
          Uri.parse("http://maps.google.com/maps?saddr="+myLocation.latitude+","+myLocation.longitude+"&daddr="+lat+","+lng + ""));
        startActivity(intent);
    }

}
