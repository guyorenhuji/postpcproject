package com.locationCoupon.yossi.locann;

import android.app.Service;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.google.android.gms.internal.lo;

import java.util.Vector;

public class ServiceDb extends SQLiteOpenHelper {

    private SQLiteDatabase db;
    private DbListeners serviceListener;
    public String decisions;

    Context mainContext;

    public final long PERIOD = 4*30*24*60;
    public final double RMSt = 60;
    public final double RMSloc = 0.001;
    public final int MAX_BETWEEN_ITERATIONS_TIME = 60;

    public ServiceDb(Context context, DbListeners serviceListener) {
        super(context, "locationAnnouncer", null, 14);
        this.serviceListener = serviceListener;
        mainContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        Vector<String> locationsColumns = new Vector<String>();
        locationsColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
        locationsColumns.add("`latitude` DOUBLE");
        locationsColumns.add("`longitude` DOUBLE");
        locationsColumns.add("`time` LONG");
        locationsColumns.add("`weight` DOUBLE");
        locationsColumns.add("`errorAll` DOUBLE");
        locationsColumns.add("`errorDay` DOUBLE");
        locationsColumns.add("`errorWeek` DOUBLE");
        createTable("locations", locationsColumns);

        Vector<String> destinationsColumns = new Vector<String>();
        destinationsColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
        destinationsColumns.add("`latitude` DOUBLE");
        destinationsColumns.add("`longitude` DOUBLE");
        destinationsColumns.add("`importance` INT");
        destinationsColumns.add("`inout` BIT");
        // 0 - inactive
        // 1 - one time reminder
        // 2 - permanent
        destinationsColumns.add("`status` INT");
        destinationsColumns.add("`name` VARCHAR(45)");
        createTable("destinations", destinationsColumns);

        Vector<String> variablesColumns = new Vector<String>();
        variablesColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
        variablesColumns.add("`key` VARCHAR(45)");
        variablesColumns.add("`value` VARCHAR(45)");
        createTable("variables", variablesColumns);

        Vector<String> logColumns = new Vector<String>();
        logColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
        logColumns.add("`timestamp` LONG");
        logColumns.add("`value` VARCHAR(45)");
        createTable("log", logColumns);

        log("db created");
        serviceListener.onDbOpened();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.db = db;
        switch(oldVersion) {
            case 11: case 10: case 12: case 13:
                dropTable("destinations");
                Vector<String> destinationsColumns = new Vector<String>();
                destinationsColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
                destinationsColumns.add("`latitude` DOUBLE");
                destinationsColumns.add("`longitude` DOUBLE");
                destinationsColumns.add("`importance` INT");
                destinationsColumns.add("`inout` BIT");
                // 0 - inactive
                // 1 - one time reminder
                // 2 - permanent
                destinationsColumns.add("`status` INT");
                destinationsColumns.add("`name` VARCHAR(45)");
                createTable("destinations", destinationsColumns);

                dropTable("locations");
                Vector<String> locationsColumns = new Vector<String>();
                locationsColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
                locationsColumns.add("`latitude` DOUBLE");
                locationsColumns.add("`longitude` DOUBLE");
                locationsColumns.add("`time` LONG");
                locationsColumns.add("`weight` DOUBLE");
                locationsColumns.add("`errorAll` DOUBLE");
                locationsColumns.add("`errorDay` DOUBLE");
                locationsColumns.add("`errorWeek` DOUBLE");
                createTable("locations", locationsColumns);

                dropTable("variables");
                Vector<String> variablesColumns = new Vector<String>();
                variablesColumns.add("`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL");
                variablesColumns.add("`key` VARCHAR(45)");
                variablesColumns.add("`value` VARCHAR(45)");
                createTable("variables", variablesColumns);
            case 14:

                break;
            default:
                throw new IllegalStateException("onUpgrade() with unknown oldVersion" + oldVersion);
        }
        log("db upgraded");
        serviceListener.onDbOpened();
    }

    @Override
    public void onOpen (SQLiteDatabase db) {
        this.db = db;
        serviceListener.onDbOpened();

    }

    public Vector<LocationProb> getLocationsProbs(int day, int hour, int min, String res, int minsRange) {
        long curTime = ServiceClock.getCurrentTimeMin();
        long timeRes = getTimeRes(res);

        Vector<LocationProb> locs = new Vector<LocationProb>();


        Cursor cursor = db.rawQuery("SELECT latitude, longitude, sum(weight) as count FROM " +
                "(" +
                "SELECT latitude, longitude, time, (("+(PERIOD-curTime)+"+time)/"+PERIOD+".0)*weight as weight FROM locations WHERE time>"+(curTime-PERIOD)+"" +
                ")" +
                " WHERE" +
                " ROUND((`time`%"+timeRes+")/(60*24)) = " + day + " AND ROUND((`time`%"+timeRes+")/60)%24 = " + hour + " AND (`time`%"+timeRes+")%60 BETWEEN " + Math.max(min-minsRange,0)+ " AND " + (min+minsRange)+" GROUP BY latitude, longitude" , null);
        if (cursor.moveToFirst()) {
            do {
                double latitude = cursor.getDouble(cursor.getColumnIndex("latitude"));
                double longitude = cursor.getDouble(cursor.getColumnIndex("longitude"));
                double count = cursor.getDouble(cursor.getColumnIndex("count"));
                locs.add(new LocationProb(0, 0, 0, count, new Coordinate(latitude, longitude, 50)));
            } while (cursor.moveToNext());
        }
        return locs;
    }

    public LocationProb getProbability(long reqTime, Coordinate reqCoord, boolean isError) {
        decisions = "";
        long curTime = ServiceClock.getCurrentTimeMin();
        double numOfSamplesWeek =0;
        double numOfSamplesDay =0;
        double numOfSamplesAll =0;
        double numOfSamplesAllLocationsWeek =0;
        double numOfSamplesAllLocationsDay =0;
        double numOfSamplesAllLocationsAll =0;
        double relevanceErrSum =0;
        double relevanceErrSampleWeek =0;
        double relevanceErrSampleDay =0;
        double relevanceErrSampleAll =0;
        Cursor cursor = db.rawQuery("SELECT `latitude`, `longitude`, `time`, `errorAll`, `errorDay`, `errorWeek`, `weight` FROM locations WHERE `time`>"+(curTime-PERIOD)
                +" ORDER BY `time`", null);
        if (cursor.moveToFirst()) {
            do {
                double lat = cursor.getDouble(cursor.getColumnIndex("latitude"));
                double lng = cursor.getDouble(cursor.getColumnIndex("longitude"));
                long t = cursor.getLong(cursor.getColumnIndex("time"));
                double weight = cursor.getDouble(cursor.getColumnIndex("weight"));
                double errorAll = cursor.getDouble(cursor.getColumnIndex("errorAll"));
                double errorDay = cursor.getDouble(cursor.getColumnIndex("errorDay"));
                double errorWeek = cursor.getDouble(cursor.getColumnIndex("errorWeek"));

                numOfSamplesWeek+=relevanceSample(curTime, t, getTimeRes("week"), reqTime, lat, reqCoord.latitude, lng, reqCoord.longitude, weight);
                numOfSamplesDay+=relevanceSample(curTime, t, getTimeRes("day"), reqTime, lat, reqCoord.latitude, lng, reqCoord.longitude, weight);
                numOfSamplesAll+=relevanceSample(curTime, t, getTimeRes("all"), reqTime, lat, reqCoord.latitude, lng, reqCoord.longitude, weight);

                numOfSamplesAllLocationsWeek+=relevanceSampleAllLocations(curTime, t, getTimeRes("week"), reqTime, weight);
                numOfSamplesAllLocationsDay+=relevanceSampleAllLocations(curTime, t, getTimeRes("day"), reqTime, weight);
                numOfSamplesAllLocationsAll+=relevanceSampleAllLocations(curTime, t, getTimeRes("all"), reqTime, weight);

                if (isError) {
                    double relevanceErr = relevanceErr(t, reqTime);

                    relevanceErrSampleWeek += relevanceErrSample(relevanceErr, errorWeek);
                    relevanceErrSampleDay += relevanceErrSample(relevanceErr, errorDay);
                    relevanceErrSampleAll += relevanceErrSample(relevanceErr, errorAll);

                    relevanceErrSum += relevanceErr;
                }

            } while (cursor.moveToNext());
        }

        double pWeek = numOfSamplesAllLocationsWeek == 0 ? 0 : numOfSamplesWeek/numOfSamplesAllLocationsWeek;
        double pDay = numOfSamplesAllLocationsDay == 0 ? 0 : numOfSamplesDay/numOfSamplesAllLocationsDay;
        double pAll = numOfSamplesAllLocationsAll == 0 ? 0 : numOfSamplesAll/numOfSamplesAllLocationsAll;

        double p = 0;

        if (isError) {
            double errorWeek = relevanceErrSum == 0 ? 0 : relevanceErrSampleWeek / relevanceErrSum;
            double errorDay = relevanceErrSum == 0 ? 0 : relevanceErrSampleDay / relevanceErrSum;
            double errorAll = relevanceErrSum == 0 ? 0 : relevanceErrSampleAll / relevanceErrSum;

            double weightWeek = weightModel(errorWeek, errorWeek, errorDay, errorAll);
            double weightDay = weightModel(errorDay, errorWeek, errorDay, errorAll);
            double weightAll = weightModel(errorAll, errorWeek, errorDay, errorAll);

            p = pWeek*weightWeek+pDay*weightDay+pAll*weightAll;

            decisions+="modelWeek: w: "+(int) (weightWeek * 100) / 100.0+" e: "+(int) (errorWeek * 100) / 100.0+" p:"+(int) (pWeek * 100) / 100.0+ "\n";
            decisions+="modelDay: w: "+(int) (weightDay * 100) / 100.0+" e: "+(int) (errorDay * 100) / 100.0+" p:"+(int) (pDay * 100) / 100.0+ "\n";
            decisions+="modelAll: w: "+(int) (weightAll * 100) / 100.0+" e: "+(int) (errorAll * 100) / 100.0+" p:"+(int) (pAll * 100) / 100.0+ "\n";
            decisions+="p: "+(int) (p * 100) / 100.0+ "\n";
        }

        return new LocationProb(pWeek,pDay,pAll,p, reqCoord);
    }

    public double weightModel(double errorModel, double errorWeek, double errorDay, double errorAll) {
        return ((1-errorWeek)+(1-errorDay)+(1-errorAll)) == 0 ? 1/3.0 : (1-errorModel)/((1-errorWeek)+(1-errorDay)+(1-errorAll));
    }

    public double relevance(long curTime, long t, long timeRes, long reqTime, double lat, double reqLat, double lng, double reqLng) {
        return gaussReduceCoefficient(curTime, t)*Math.exp(-(gaussExpTime(t, timeRes, reqTime) + gaussExpLoc(lat, reqLat) + gaussExpLoc(lng, reqLng)));
    }

    public double relevanceSample(long curTime, long t, long timeRes, long reqTime, double lat, double reqLat, double lng, double reqLng, double weight) {
        return relevance(curTime, t, timeRes, reqTime, lat, reqLat, lng, reqLng)*weight;
    }

    public double relevanceAllLocations(long curTime, long t, long timeRes, long reqTime) {
        return gaussReduceCoefficient(curTime, t)*Math.exp(-(gaussExpTime(t, timeRes, reqTime)));
    }

    public double relevanceSampleAllLocations(long curTime, long t, long timeRes, long reqTime, double weight) {
        return relevanceAllLocations(curTime, t, timeRes, reqTime)*weight;
    }

    public double relevanceErr(long t, long reqTime) {
        return Math.exp(-(gaussExpTime(t, getTimeRes("week"), reqTime)));
    }

    public double relevanceErrSample(double relevanceErr, double error) {
        return relevanceErr*error;
    }

    public double gaussReduceCoefficient(long curTime, long t) {
        return ((PERIOD-curTime+t)/(double)PERIOD);
    }

    public double gaussExpTime(long t, long timeRes, long reqTime) {
        return Math.pow((t % timeRes) - (reqTime % timeRes), 2)/(2.0*Math.pow(RMSt, 2));
    }

    public double gaussExpLoc(double loc, double reqLoc) {
        return Math.pow(loc-reqLoc, 2)/(2.0*Math.pow(RMSloc, 2));
    }

    public Vector<ServiceIterationFreq> getIterationFreq(String res) {
        long timeRes = getTimeRes(res);
        Vector<ServiceIterationFreq> freqs = new Vector<ServiceIterationFreq>();
        Cursor cursor = db.rawQuery("SELECT count(*) as count, time/"+timeRes+" as time FROM locations GROUP BY time ORDER BY time DESC", null);
        if (cursor.moveToFirst()) {
            do {
                int time = cursor.getInt(cursor.getColumnIndex("time"));
                int count = cursor.getInt(cursor.getColumnIndex("count"));
                System.out.println(time);
                System.out.println(count);
                freqs.add(new ServiceIterationFreq(time, count));
            } while (cursor.moveToNext());
        }

        return freqs;
    }
	
	private long getTimeRes(String res) {
        int timeRes;
        if (res.compareTo("all")==0) {
            timeRes = 1;
        } else if (res.compareTo("day")==0) {
            timeRes = 60*24;
        } else {
            timeRes = 60*24*7;
        }

		return timeRes;
	}
	
	public void addVariable(String key, String value) {
        Cursor cursor = db.rawQuery("SELECT count(*) as count FROM variables WHERE `key` = '" + key + "'", null);
        if (cursor.moveToFirst()) {
            int count = cursor.getInt(cursor.getColumnIndex("count"));
            if (count==0){
                db.execSQL("INSERT INTO variables (key, value) VALUES ('" + key + "', '" + value + "')");
            } else {
                db.execSQL("UPDATE variables SET value = '" + value + "' WHERE `key` = '" + key + "'");
            }
        }
	}

    public void log(String value) {
        System.out.println(value);
        db.execSQL("INSERT INTO log (timestamp, value) VALUES ('" + ServiceClock.getCurrentTimeMili() + "', '" + value + "')");
    }
	
	public String getVariable(String key) {
		String value = null;
        Cursor cursor = db.rawQuery("SELECT `value` FROM variables WHERE `key` = '"+key+"'", null);
        if (cursor.moveToFirst()) {
            value = cursor.getString(cursor.getColumnIndex("value"));
        }

		return value;
	}
	
	public void saveLocation(Coordinate coord, long curTime) {
        decisions = "";
        LocationProb prob = getProbability(curTime, coord, false);
        decisions+="LocationProb: a:"+(int) (prob.pAll * 100) / 100.0+" d:"+(int) (prob.pDay * 100) / 100.0+" w:"+(int) (prob.pWeek * 100) / 100.0+ "\n";
        long lastTime = curTime;
        Cursor cursor = db.rawQuery("SELECT `id`, `time`, `weight` FROM locations ORDER BY `time` DESC LIMIT 1", null);
        if (cursor.moveToFirst()) {
            lastTime = cursor.getLong(cursor.getColumnIndex("time"));
            double weight = cursor.getDouble(cursor.getColumnIndex("weight"));
            int id = cursor.getInt(cursor.getColumnIndex("id"));
            db.execSQL("UPDATE locations SET `weight` = '" + (weight+(0.5*(curTime-lastTime)/(double)MAX_BETWEEN_ITERATIONS_TIME)) + "' WHERE `id`='" + id + "'");

        }
        db.execSQL("INSERT INTO locations (`latitude`, `longitude`, `time`, `errorAll`, `errorDay`, `errorWeek`, `weight`) VALUES ('" +
                coord.latitude + "', '" + coord.longitude + "', '" + curTime + "' , '" + (1-prob.pAll)+ "' , '" + (1-prob.pDay)+ "' , '" + (1-prob.pWeek)+ "' , '" + (0.5*(curTime-lastTime)/(double)MAX_BETWEEN_ITERATIONS_TIME) + "')");
	}

    public Vector<Destination> getAllDestinations(){
		Vector<Destination> dests = new Vector<Destination>();
        Cursor cursor = db.rawQuery("SELECT * from destinations", null);
        if (cursor.moveToFirst()) {
            do {
                Coordinate coor = new Coordinate(cursor.getDouble(cursor.getColumnIndex("latitude")), cursor.getDouble(cursor.getColumnIndex("longitude")), 50);
                dests.add(new Destination(coor, cursor.getInt(cursor.getColumnIndex("importance")), intToBool(cursor.getInt(cursor.getColumnIndex("inout"))), cursor.getInt(cursor.getColumnIndex("status")), cursor.getString(cursor.getColumnIndex("name")), cursor.getInt(cursor.getColumnIndex("id"))));
            } while (cursor.moveToNext());
        }

		return dests;
	}

    public void clearAllDestinations(){
        db.execSQL("DELETE FROM destinations");
    }

    public Destination getDestination(int id){
        Cursor cursor = db.rawQuery("SELECT * from destinations WHERE `id` = '"+id+"'", null);
        if (cursor.moveToFirst()) {
                Coordinate coord = new Coordinate(cursor.getDouble(cursor.getColumnIndex("latitude")), cursor.getDouble(cursor.getColumnIndex("longitude")), 50);
                return new Destination(coord, cursor.getInt(cursor.getColumnIndex("importance")), intToBool(cursor.getInt(cursor.getColumnIndex("inout"))), cursor.getInt(cursor.getColumnIndex("status")), cursor.getString(cursor.getColumnIndex("name")), cursor.getInt(cursor.getColumnIndex("id")));
        }

        return null;
    }

    private boolean intToBool( int i ){
        if (i==0) {
            return false;
        }
        return true;
    }

    private int boolToInt( boolean i ){
        if (i) {
            return 1;
        }
        return 0;
    }
	
	public void addDestination(Destination destination){
        db.execSQL("INSERT INTO destinations (`latitude`, `longitude`, `importance`, `inout`, `status`, `name`) VALUES ('"+destination.coord.latitude+"', '"+destination.coord.longitude+"', '"+destination.importance+"', '"+boolToInt(destination.inOut)+"', '"+destination.status+"', '"+destination.name+"')");
	}

    public void saveDestination(Destination destination){
        if (destination.id!=-1) {
            db.execSQL("UPDATE destinations SET `latitude` = '" + destination.coord.latitude + "', `longitude` = '" + destination.coord.longitude + "', `importance` = '" + destination.importance + "', `inout` = '" + boolToInt(destination.inOut) + "', `status` = '" + destination.status + "', `name` = '" + destination.name + "' WHERE `id`='" + destination.id + "'");
        } else {
            log("cannot save destination with id = -1");
        }
    }
	
	public void removeDestination(int destinationId){
        db.execSQL("DELETE FROM destinations WHERE `id`="+destinationId);
	}
	
	private void createTable(String name, Vector<String> columns) {
        String createTable = "CREATE TABLE IF NOT EXISTS "+name+"(";
        for (String column : columns) {
            createTable+= column+", ";
        }
        createTable = createTable.substring(0, createTable.length()-2);
        createTable+=");";
        db.execSQL(createTable);
	}

    public void dropTable(String name) {
        db.execSQL("DROP TABLE IF EXISTS "+name+"");
    }
	
	public String printDb() {
        String out = "";
        out+="===destinations===\n";
        Cursor cursor = db.rawQuery("SELECT * from destinations ORDER BY `id`", null);
        if (cursor.moveToFirst()) {
            do {
                out+="id: " + cursor.getInt(cursor.getColumnIndex("id")) + " latitude: " + cursor.getDouble(cursor.getColumnIndex("latitude")) + " longitude: " + cursor.getDouble(cursor.getColumnIndex("longitude")) + " importance: " + cursor.getDouble(cursor.getColumnIndex("importance")) + " status: " + cursor.getInt(cursor.getColumnIndex("status"))+ " inout: " + cursor.getInt(cursor.getColumnIndex("inout"))+"\n";
            } while (cursor.moveToNext());
        }
        out+="===variables===\n";
        cursor = db.rawQuery("SELECT * from variables ORDER BY `id`", null);
        if (cursor.moveToFirst()) {
            do {
                out+="id: " + cursor.getInt(cursor.getColumnIndex("id")) + " key: " + cursor.getString(cursor.getColumnIndex("key")) + " value: " + cursor.getString(cursor.getColumnIndex("value"))+"\n";
            } while (cursor.moveToNext());
        }
        out+="===average===\n";
        int count=0;
        long first=0, last=0;
        cursor = db.rawQuery("SELECT count(*) as count from locations ", null);
        if (cursor.moveToFirst()) {
            count = cursor.getInt(cursor.getColumnIndex("count"));
        }
        cursor = db.rawQuery("SELECT * from locations ORDER BY `id` ASC LIMIT 1 ", null);
        if (cursor.moveToFirst()) {
            first = cursor.getLong(cursor.getColumnIndex("time"));
        }
        cursor = db.rawQuery("SELECT * from locations ORDER BY `id` DESC LIMIT 1 ", null);
        if (cursor.moveToFirst()) {
            last = cursor.getLong(cursor.getColumnIndex("time"));
        }
        out+="total iterations: "+count+"\n";
        out+="total days: "+((last-first)/(60*24))+"\n";
        out+="avarage per day: "+(count/((last-first)/(60*24)))+"\n";
        out+="===locations===\n";
        cursor = db.rawQuery("SELECT * from locations ORDER BY `id` DESC LIMIT 100 ", null);
        if (cursor.moveToFirst()) {
            do {
                out+="id: " + cursor.getInt(cursor.getColumnIndex("id")) + " latitude: " + cursor.getDouble(cursor.getColumnIndex("latitude")) + " longitude: " + cursor.getDouble(cursor.getColumnIndex("longitude")) + " time: " + cursor.getLong(cursor.getColumnIndex("time")) +"\n";
                out+="errorWeek: " + cursor.getDouble(cursor.getColumnIndex("errorWeek")) + " errorDay: " + cursor.getDouble(cursor.getColumnIndex("errorDay")) + " errorAll: " + cursor.getDouble(cursor.getColumnIndex("errorAll")) + " weight: " + cursor.getDouble(cursor.getColumnIndex("weight")) +"\n";
            } while (cursor.moveToNext());
        }

        return out;
	}

    public String printLog() {
        String out = "";
        Cursor cursor = db.rawQuery("SELECT * from log ORDER BY `timestamp` DESC LIMIT 300", null);
        if (cursor.moveToFirst()) {
            do {
                out += ServiceClock.getHumanTime(cursor.getLong(cursor.getColumnIndex("timestamp"))) + " : " +  cursor.getString(cursor.getColumnIndex("value"))+"\n";
            } while (cursor.moveToNext());
        }
        return out;
    }
}