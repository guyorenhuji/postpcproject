package com.locationCoupon.yossi.locann;

public class Coordinate implements Comparable<Coordinate>{
    public double longitude;
    public double latitude;
	public int radius;

	/*
	 * latitude and longitude in decimal degrees
	 * radius in meters
	 */
	public Coordinate(double latitude, double longitude, int radius){
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
	}

	public int compareTo(Coordinate o) {
        return ServiceDistance.getDistanceInMeters(this, o)-this.radius-o.radius;
	}
	
	public String toString() {
		return latitude+" : "+longitude+" - "+radius;
	}
}
