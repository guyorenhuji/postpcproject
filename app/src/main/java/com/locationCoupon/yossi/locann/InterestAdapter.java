package com.locationCoupon.yossi.locann;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class InterestAdapter extends ArrayAdapter<Interest> {
    public InterestAdapter(Context context, int textViewResourceId, List<Interest> objects) {
        super(context, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.interest_layout, parent, false);
        //get objects
        TextView name = (TextView)view.findViewById(R.id.name);
        ImageView image = (ImageView)view.findViewById(R.id.imageView);
        name.setText(getItem(position).name);
        if (getItem(position).isInterested) {
            name.setTextColor(Color.parseColor("#25214b"));
            image.setBackgroundResource(R.drawable.switch_on);
        }
        else {
            name.setTextColor(Color.GRAY);
            image.setBackgroundResource(R.drawable.switch_off);
        }
        return view;
    }
}