package com.locationCoupon.yossi.locann;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.nio.DoubleBuffer;
import java.util.Vector;


public class ServiceManagerBroadcastReceiver extends BroadcastReceiver implements DbListeners, LocationListeners, DistanceListeners {

	private Context context;
    private ServiceDb db;
    private ServiceLog log;
    private ServiceLocation location;
    private Coordinate lastCoord, curCoord;
    private double requiredAccuracy;
    private long startTimeMili;
    private String decisions;
    private int minTime, numOfDests, destCounter;
    private Lock lock;

    @Override
	public void onReceive(Context context, Intent intent) {
		this.context = context.getApplicationContext();

        log = new ServiceLog(context);
        Toast.makeText(context, "Start iteration", Toast.LENGTH_SHORT).show();
        log.log("Start iteration");
        decisions = "";
        minTime = 60*60;
        destCounter = 0;
        lock = new Lock();

        setAlarm(context, 300);

        db = new ServiceDb(context, this);
        db.getWritableDatabase();
	}

	public void setAlarm(Context context, int seconds) {
        if (location!=null) {
            location.stopLocationService();
        }

		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, ServiceManagerBroadcastReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 1, intent, 0);
		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000* seconds, pi);

        log = new ServiceLog(context);
        Toast.makeText(context, "Set alarm in " + ServiceClock.getHumanTime(ServiceClock.getCurrentTimeMili() + 1000* seconds), Toast.LENGTH_SHORT).show();
        log.log("Set alarm in " + ServiceClock.getHumanTime(ServiceClock.getCurrentTimeMili() + 1000* seconds));
	}

	public void cancelAlarm(Context context) {
        if (location!=null) {
            location.stopLocationService();
        }

		Intent intent = new Intent(context, ServiceManagerBroadcastReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(context, 1, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);

        log = new ServiceLog(context);
		log.log("Cancel all alarms");
	}

	public void getArrivalTimes() {
        Vector<Destination> destinations = db.getAllDestinations();
        decisions+="Time: "+ServiceClock.getHumanTime()+"\n";
        numOfDests = destinations.size();
        decisions+="Number of destinations: "+numOfDests+"\n";
        for (Destination destination : destinations) {
            ServiceDistance dir = new ServiceDistance(this);
            dir.getArrivalTimeInSec(curCoord, destination);
        }

        if(numOfDests==0) {
            setAlarm(context, 60*60);
        }
    }

    @Override
    public void onDbOpened() {
        startTimeMili = ServiceClock.getCurrentTimeMili();
        requiredAccuracy = db.getVariable("requiredAccuracy") == null ? 1000 :  Double.valueOf(db.getVariable("requiredAccuracy"));
        log.log("required accuracy: "+requiredAccuracy);
        location = new ServiceLocation(context, startTimeMili, requiredAccuracy, this);
    }

    @Override
    public void onLocationReceived(Coordinate coord) {
        curCoord = coord;
        db.saveLocation(coord, startTimeMili / (1000 * 60));
        log.log(db.decisions);
        log.log("saved location");
        if (db.getVariable("lastCoordLat")!=null) {
            lastCoord = new Coordinate(Double.parseDouble(db.getVariable("lastCoordLat")), Double.parseDouble(db.getVariable("lastCoordLong")), Integer.valueOf(db.getVariable("lastCoordRadius")));
        } else {
            lastCoord = new Coordinate(curCoord.latitude, curCoord.longitude, curCoord.radius);
        }
        log.log("last coord: "+lastCoord.latitude+", "+lastCoord.longitude);
        db.addVariable("lastCoordLat", String.valueOf(curCoord.latitude));
        db.addVariable("lastCoordLong", String.valueOf(curCoord.longitude));
        db.addVariable("lastCoordRadius", String.valueOf(curCoord.radius));

        getArrivalTimes();
    }

    @Override
    public void onLocationFailed() {
        setAlarm(context, 60*5);
    }

    public void notifyUser(Destination dest) {
        log.log("notify user");
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, ServiceManagerActivity.class), 0);
        String detail = "you have an hot coupon near you, click to get it!!!";
        String description = "";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(detail)
                        .setContentText(description);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }


    @Override
    public void onDistanceReceived(int arrivalInSec, int distanceInMeters, Destination destination) {
        try {
            lock.lock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ++destCounter;
        log.log("destCounter: "+destCounter);
//        if (destination.status!=0) {
            decisions += "----" + destination.name + "----\n";
            decisions += "\t when: " + (destination.inOut ? "in" : "out") + "\n";
            decisions += "\t status: " + destination.status + "\n";
            decisions += "\t distance: " + (int) ((distanceInMeters / 1000.0) * 100) / 100.0 + " Km\n";
            decisions += "\t arrivalIn: " + arrivalInSec / 60 + "mins \n";

            destination.status = destination.status==1 ? 0 : 2;
            db.saveDestination(destination);

            long arrivalTimeInSec = arrivalInSec + startTimeMili / 1000;
            LocationProb prob = db.getProbability(arrivalTimeInSec / 60, destination.coord, true);
            double arrivalProbability = prob.p;

            decisions += "\t arrivalProbability: " + (int) (arrivalProbability * 100) / 100.0 + "\n";

            if (distanceInMeters < 1000) {
                decisions += "\t in place, ";
                if (lastCoord.compareTo(destination.coord) > 500) {
                    notifyUser(destination);
                    decisions += "notify!\n";
                } else {
                    decisions += "already notified!\n";
                }

            } else {
                minTime = Math.min((int) (arrivalInSec * (1.5 - arrivalProbability)), minTime);
                decisions += "\t set timer in: " + (int) (arrivalInSec * (1.5 - arrivalProbability)) / 60 + "mins \n";
            }

            decisions += db.decisions;

            if (numOfDests == destCounter) {
                decisions += "final set timer in: " + minTime / 60 + "mins \n";
                db.addVariable("decisions", decisions);
                log.log(decisions);

                setAlarm(context, Math.max(minTime, 60));
            }
//        }
        lock.unlock();
    }
}