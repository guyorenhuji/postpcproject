package com.locationCoupon.yossi.locann;

public class Interest {
    public String name;
    public boolean isInterested;
    public String id;
    public Interest(String id, String name)
    {
        this.id = id;
        this.name = name;
        isInterested = false;
    }
}
