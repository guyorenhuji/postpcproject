package com.locationCoupon.yossi.locann;

/**
 * Created by yossi on 3/11/2015.
 */
public class LocationProb {
    public double pWeek;
    public double pDay;
    public double pAll;
    public double p;
    public int id;
    public Coordinate coord;

    public LocationProb(double pWeek, double pDay, double pAll, double p, Coordinate coord) {
        this.pWeek = pWeek;
        this.pDay = pDay;
        this.pAll = pAll;
        this.p = p;
        this.coord = coord;
    }

    public LocationProb(double pWeek, double pDay, double pAll, double p, Coordinate coord, int id) {
        this.pWeek = pWeek;
        this.pDay = pDay;
        this.pAll = pAll;
        this.p = p;
        this.coord = coord;
        this.id = id;
    }
}
