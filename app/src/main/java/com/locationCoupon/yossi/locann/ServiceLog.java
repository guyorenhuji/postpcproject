package com.locationCoupon.yossi.locann;

import android.content.Context;

/**
 * Created by yossi on 1/2/2015.
 */
public class ServiceLog implements DbListeners {

    private Context context;
    private ServiceDb db;
    private boolean isDbOpened;
    private String value;

    public ServiceLog(Context context) {
        this.context = context;
        this.value = null;
        db = new ServiceDb(context, this);
        db.getWritableDatabase();
    }

    public void log(String value) {
        if (isDbOpened) {
            db.log(value);
        } else {
            this.value = value;
        }
    }

    @Override
    public void onDbOpened() {
        isDbOpened = true;
        if (value!=null) {
            db.log(value);
        }
    }
}
